export default function () {
    const test = ref('test')
    const edit = (value) => {
        test.value = value
    }

    return {
        test,
        edit
    }
}
